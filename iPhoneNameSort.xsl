<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl">

  <xsl:script><![CDATA[
    function formatPrice(price)
    {
      var l = price.length;
      if (l > 3)
        price = price.substr(0,l-3) + "," + price.substr(l-3)
      return  price + " ₽";
    }


  ]]></xsl:script>

  <xsl:template match="/">
    <TABLE STYLE="border:1px solid black">
      <TR  STYLE="font-size:12pt; font-family:Verdana; font-weight:bold; text-decoration:underline">
        <TD>NAME</TD>
        <TD>PRICE</TD>
        <TD>SALER</TD>
        <TD>COLOR</TD>
        <TD>MEMORY</TD>
      </TR>
      <xsl:for-each select="PRODUCTS/PRODUCT" order-by="NAME">
        <TR STYLE="font-family:Verdana; font-size:12pt; padding:0px 6px">
          <TD STYLE="padding: 10px;"><xsl:value-of select="NAME"/></TD>
          <TD STYLE="padding: 10px;"><xsl:apply-templates select="PRICE"/></TD>
          <TD STYLE="padding: 10px;"><xsl:value-of select="SALER"/></TD>
          <TD STYLE="padding: 10px;"><xsl:value-of select="COLOR"/></TD>
          <TD STYLE="padding: 10px;"><xsl:value-of select="MEMORY"/></TD>
        </TR>
      </xsl:for-each>
    </TABLE>
  </xsl:template>
  
  <xsl:template match="PRICE">
    <xsl:eval>formatPrice(this.text)</xsl:eval>
  </xsl:template>

  <!-- <xsl:template match="DATE">
    <xsl:eval>this.text</xsl:eval>
  </xsl:template> -->

</xsl:stylesheet>
