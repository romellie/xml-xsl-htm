===================================
XML -- Multiple Views with XML/XSL
===================================
Last Updated: March24, 2001.


SUMMARY
========
This demo allows the user to surf through different XML documents 
and apply different XSL stylesheets to those documents. Choose an 
XML document and a XSL stylesheet on the left and display the data 
on the right.  
       

DETAILS
========
This demo is a good example of using XSL to offer different views 
on XML. Many times, the web designer will want to display different 
data to different consumers. Through XSL this can be done without 
altering the original XML document or the HTML page used to load that 
document.  

SCENARIO
=========
This is a way to look at all single sales of over $5,000 for three
products in the Adventure Works organization.

USAGE
======
This validator can be used with both XML schemas and DTDs. 
 
 
BROWSER/PLATFORM COMPATIBILITY
===============================
You must be running Microsoft Internet Explorer 5 or greater on 
Win32 or Unix platforms to view the demo.    


SOURCE FILES
=============
controls_js.htm
controls_vbs.htm
Multiple_js.htm
Multiple_vbs.htm
results_js.htm
results_vbs.htm
kodiak.xml
avalanche.xml
eagle.xml
sales-table.xsl
sales-table2.xsl
sales-graph.xsl
raw-xml.xsl
sort-customer.xsl
sort-customer-.xsl
sort-cust+price.xsl
sort-cust+price-.xsl
top-sale.xsl

OTHER FILES
============
burl-s.gif
onio-s.gif
sand-s.gif
swallows.jpg


SEE ALSO
=========
MSDN Online Web Workshop - XSL Elements:
http://msdn.microsoft.com/xml/reference/xsl/xslelements.asp

MSDN Online Web Workshop - XSL Developer's Guide:
http://msdn.microsoft.com/xml/XSLGuide/default.asp

MSDN Online Web Workshop - XML:
http://msdn.microsoft.com/workshop/xml/index.asp

MSDN Online - XML Developer Center:
http://msdn.microsoft.com/xml/default.asp

==================================
� Microsoft Corporation 1999-2001
